﻿using System.Security.Principal;
namespace baiktbuoi7web.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactAndAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }
}
